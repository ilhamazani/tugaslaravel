@extends('layouts.main')

@section('title', 'Home')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Pertanyaan</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Pertanyaan</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">
			<a href="{{ url('pertanyaan/create') }}"><button type="button" class="btn btn-success ml-5">+</button></a>
			<table class="table">
				<thead>
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Judul</th>
						<th scope="col">Isi</th>
						<th scope="col">Created At</th>
						<th scope="col">Updated At</th>
						<th scope="col">Aksi</th>
					</tr>
				</thead>
				<tbody>
					@foreach($pertanyaan as $row)
					<tr>
						<td>{{ $row->id }}</td>
						<td>{{ $row->judul }}</td>
						<td>{{ $row->isi }}</td>
						<td>{{ $row->created_at }}</td>
						<td>{{ $row->updated_at }}</td>
						<td>
							<a href="{{ url('pertanyaan/'. $row->id) }}" class="btn btn-warning btn-icon"><i class="fa fa-pencil"></i>Show</a>
							<a href="{{ url('pertanyaan/'. $row->id.'/edit') }}" class="btn btn-primary btn-icon"><i class="fa fa-pencil"></i>Edit</a>
							<form action="{{ url('pertanyaan/'.$row->id)}}" method="post">
								@csrf
								<input type="hidden" name="_method" value="DELETE">
								<button>Delete</button>
							</form>
						</i></a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<!-- /.row (main row) -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

@endsection

