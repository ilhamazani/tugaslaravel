@extends('layouts.main')

@section('title', 'Home')

@section('content')
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<div class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<h1 class="m-0 text-dark">Add Pertanyaan</h1>
				</div><!-- /.col -->
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">Add Pertanyaan</li>
					</ol>
				</div><!-- /.col -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</div>
	<!-- /.content-header -->

	<!-- Main content -->
	<section class="content">
		<div class="container-fluid">				
			<form action="{{ url('pertanyaan/'.$pertanyaan->id) }}" method="post">
    				{{ csrf_field() }}
    				<input type="hidden" name="_method" value="PUT">
					<div class="form-group">
						<label for="exampleFormControlInput1">Judul</label>
						<input type="text" class="form-control" id="exampleFormControlInput1" name="form_judul" value="{{ $pertanyaan->judul }}" placeholder="Masukkan Judul">
					</div>
					<div class="form-group">
						<label for="exampleFormControlTextarea1">Isi</label>
						<textarea class="form-control" id="exampleFormControlTextarea1" name="form_isi" rows="3">{{ $pertanyaan->isi }}</textarea>
					</div>
					<input type="submit" name="save" value="Simpan" class="btn btn-primary" style="margin-right: 10px">
    				<input type="reset" name="batal" value="Batal" class="btn btn-danger">
				</form>
			
		</div>
		<!-- /.row (main row) -->
	</div><!-- /.container-fluid -->
</section>
<!-- /.content -->
</div>

@endsection

