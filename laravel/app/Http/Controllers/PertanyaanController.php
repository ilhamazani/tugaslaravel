<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PertanyaanController extends Controller
{
    public function index()
    {
    	$pertanyaan = DB::table('pertanyaan')->get();
    	
    	return view('pertanyaan', ['pertanyaan' => $pertanyaan]);
    }

    public function create()
    {
        return view('add-pertanyaan'); 
    }

    public function store(Request $request)
    {
        DB::table('pertanyaan')->insert([
        'judul' => $request->form_judul,
        'isi' => $request->form_isi,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
  	]);
        return redirect('pertanyaan'); 
    }

    public function show($id)
    {
    	$data['pertanyaan'] = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('detail-pertanyaan', $data);
    }



    public function edit($id)
    {
        $data['pertanyaan'] = DB::table('pertanyaan')->where('id', $id)->first();
        return view('edit-pertanyaan', $data);
    }

    public function update(Request $request, $id)
    {
    	DB::table('pertanyaan')->where('id', $id)->update([
    		'judul' => $request->form_judul,
    		'isi' => $request->form_isi,
    		'updated_at' => \Carbon\Carbon::now()
    	]);
    	return redirect('pertanyaan');
    }

    public function destroy($id)
    {
  		DB::table('pertanyaan')->where('id',$id)->delete();
    	return redirect('pertanyaan');
    }
}
